from kafka import KafkaProducer
import json
import random
from time import sleep
from datetime import datetime

# Create an instance of the Kafka producer
producer = KafkaProducer(bootstrap_servers='localhost:9092',
                            value_serializer=lambda v: str(v).encode('utf-8'))
def gerador_mensagens_kafka():
    print("Pressione control+C para sair")
    while True:
        producer.send('meu_topico', random.randint(1,999))

gerador_mensagens_kafka()