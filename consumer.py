from kafka import KafkaConsumer
from pymongo import MongoClient
from json import loads


# consumer = KafkaConsumer(
#     'numtest',
#      bootstrap_servers=['localhost:9092'],
#      auto_offset_reset='earliest',
#      enable_auto_commit=True,
#      group_id='my-group',
#      value_deserializer=lambda x: loads(x.decode('utf-8')))


consumer = KafkaConsumer(
    'meu_topico',
     bootstrap_servers=['localhost:9092'],
     auto_offset_reset='earliest',
     enable_auto_commit=True,
     value_deserializer=lambda x: loads(x.decode('utf-8')))


client = MongoClient('localhost:27017', username='root', password='root')

collection = client.pymodb.meuTopico


for message in consumer:
    reg = {'valor':message.value}

    collection.insert_one(reg)
    print('{} adicionado a {}'.format(reg, collection))

